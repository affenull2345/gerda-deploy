#!/bin/sh
#
# GerdaOS App Deployment script
#

if [ $# -gt 0 ]; then
	echo "GerdaOS App Deployment script"
	echo "Please run this script without arguments in a" \
		"B2G/KaiOS app directory."
	exit 1
fi

err () {
	printf 'E: %s\n' "$@"
}

note () {
	printf 'N: %s\n' "$@"
}

if [ ! -f manifest.webapp ]; then
	err "Missing manifest file."
	note "Please run this script in a B2G/KaiOS app directory."
	exit 2
fi

ORIGIN=$( (cat manifest.webapp && echo ) | (while read -r ml; do
	origin=$(echo "$ml" | sed 's/^.*"origin": *"\([^"]*\)".*$/\1/')
	if [ "x$ml" != "x$origin" ]; then
		# Found
		echo "$origin"
		break
	fi
done) )

if [ -z "$ORIGIN" ]; then
	err "Missing origin definition in manifest file."
	note "It is required for a valid manifest URL."
	note "Alternatively, pass the ORIGIN environment variable to the script."
	exit 3
fi

echo "$ORIGIN"

tmpdir=$(mktemp -d)

if [ -f .gerda-deploy.ignore ]; then
	zip -r "$tmpdir/application.zip" -x@.gerda-deploy.ignore -- *
else
	zip -r "$tmpdir/application.zip" -- *
fi
cat <<END > "$tmpdir/metadata.json"
{
	"version": 1,
	"manifestURL": "$ORIGIN/manifest.webapp"
}
END
cd "$tmpdir"
zip package.zip metadata.json application.zip
client () {
	read -r version
	case "$version" in
		telnet*)
			echo "Failed to connect to helper. Is it open?" 1>&2
			kill -ABRT $$
			;;
		"App deployment helper v2"*)
			;;
		*)
			echo "Invalid version: $version" 1>&2
			kill -ABRT $$
	esac
	echo "$ORIGIN"
	read -r response
	echo "$response" 1>&2
	# Bug in ADB: it echoes input back to output. Maybe a port forward
	# would be better?
	case "$response" in
		"$ORIGIN"*)
			read -r response
			echo "$response" 1>&2
			;;
	esac
	case "$response" in
		Uninstall*)
			echo "App requires uninstall. Proceed (y/n)?" > \
				/dev/tty
			read -r answer < /dev/tty
			case "$answer" in
				y*|Y*)
					echo "uninstall"
					;;
				*)
					kill -ABRT $$
					;;
			esac
			;;
	esac
}
server () {
	adb shell busybox telnet 127.0.0.1:4444
}
if ! adb push package.zip /data/media/deployment.tmp.zip; then
	echo "Failed to send package. Is the device connected?"
else
	mkfifo /tmp/gerda-deploy.pipe.$$
	(server </tmp/gerda-deploy.pipe.$$) | \
		(client >/tmp/gerda-deploy.pipe.$$)
fi
cd /
trap "rm -rf '/tmp/gerda-deploy.pipe.$$'; rm -rf '$tmpdir'" EXIT
trap "echo Aborted; exit 3" INT TERM ABRT
