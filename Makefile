PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
BINNAME = gerda-deploy

all:
	zip application.zip *.html *.css *.js icon_*.png manifest.webapp
	zip package.zip application.zip metadata.json

clean:
	rm -f application.zip package.zip

install:
	install deploy.sh "$(DESTDIR)$(BINDIR)/$(BINNAME)"
