# GerdaOS app deployment script

This is a shell script and an app for easy deployment of apps to a
GerdaOS device. Deploying an app is as simple as running

    $ gerda-deploy

in the app's source directory. The "Deployment Helper" app has to be running on
the phone, which should be connected via USB.

Files that should not be included in the app can
be added to the `.gerda-deploy.ignore` file. The file is in `zip`'s exclude
format - see its documentation for more information on how this file works.

## Installing

Execute this in your shell:

    $ make
    $ sudo make install

and copy package.zip to the phone. Install it using the file manager.

`make install` also supports the PREFIX variable. If your system supports it,
you can install for the current user only with:

    $ make install PREFIX=$HOME/.local

## Bugs or problems

 - Apps can't be updated without deleting their data.
 - Sometimes, the app gets cached and you need to reboot to see the updates.
