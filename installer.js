function log(text, type){
	var el = document.body.appendChild(
		document.createElement('div'));
	el.textContent = text;
	if(type == 'error') el.style.color = 'red';
	if(type == 'success') el.style.color = 'green';
	el.scrollIntoView();
}
function makeAppForeground(){
	return new Promise(function(resolve, reject){
		navigator.mozApps.getSelf().onsuccess = function(){
			this.result.launch();
			resolve();
		}
	});
}
function uninstallPackage(app){
	return new Promise(function(resolve, reject){
		navigator.mozApps.mgmt.uninstall(app);
		resolve();
	});
}
function checkPackage(origin){
	return new Promise(function(resolve, reject){
		var allReq = navigator.mozApps.mgmt.getAll();
		allReq.onsuccess = function(){
			var installed = false;
			this.result.forEach(function(app){
				if(app.manifestURL == origin +
					'/manifest.webapp')
				{
					log('App already installed: ' +
						origin, 'error');
					reject(app);
					installed = true;
				}
			});
			if(!installed){
				log('App is not yet installed: ' + origin,
					'success');
				resolve();
			}
		}
	});
}
function installPackage(origin){
	log('Installing ' + origin + '...');
	var storage = navigator.getDeviceStorages('sdcard')[0];
	var req = storage.get('deployment.tmp.zip');
	req.onsuccess = function(){
		log('File found', 'success');
		navigator.mozApps.mgmt.import(req.result).then(function(app){
			log('Installed app!', 'success');
			app.launch();
		}).catch(function(e){
			log('Installation error: ' + e.name, 'error');
		});
	}
	req.onerror = function(){
		log('Failed to get .../deployment.tmp.zip', 'error');
	}
}
var sock = navigator.mozTCPSocket.listen(4444, {binaryType: 'string'});
sock.onconnect = function(conn){
	log('Connection');
	var orig, oldapp;
	var data = '';
	var step = 'origin';
	conn.socket.send('App deployment helper v2\r\n');
	conn.socket.ondata = function(e){
		data += e.data;
		if(data.indexOf('\n') > 0){
			switch(step){
				case 'origin':
					orig = data.replace(/\r?\n/, '');
					checkPackage(orig).then(function(){
						installPackage(orig);
						conn.socket.close();
					}).catch(function(app){
						oldapp = app;
						conn.socket.send(
							'Uninstall?\r\n');
						step = 'uninstall';
						data = '';
					});
					break;
				case 'uninstall':
					if(data == 'uninstall\n' ||
						data == 'uninstall\r\n')
					{
						makeAppForeground()
						.then(function(){
							log('Uninstalling...');
							return uninstallPackage(
								oldapp);
						}).then(function(){
							installPackage(orig);
							conn.socket.close();
						});
					}
					break;
			}
		}
	}
}
